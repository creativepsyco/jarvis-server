global.P = require("bluebird");
P.promisifyAll(require("mongoose"));
var autoIncrement = require('mongoose-auto-increment');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var users = require('./routes/users');
var tokens = require('./routes/tokens');
var jars = require('./routes/jars');
var patients = require('./routes/patients');
var gcm_tokens = require('./routes/device_tokens');
var wts = require('./routes/weights.js');
var payment = require('./payment');
var controllers = require('./routes/controllers');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.send('API is running');
});
app.use('/users', users);
app.use('/tokens', tokens);
app.use('/jars', jars);
app.use('/controllers', controllers);
app.use('/payment', payment);
app.use('/patients', patients);
app.use('/weights', wts);
app.use('/gcm_tokens', gcm_tokens);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

var mongoose = require('mongoose');
mongoose.connectAsync('mongodb://localhost/Users/mohit/test')
	.then(function() {
		autoIncrement.initialize(mongoose.connection);
		console.log('connection successful');
	}).catch(function(err) {
		console.log('connection error', err);
	});

var listener = app.listen(3000, function() {
	console.log('Listening on port ' + listener.address().port); //Listening on port 3000
});

module.exports = app;
