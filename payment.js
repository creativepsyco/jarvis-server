var express = require('express');
var router = express.Router();

var braintree = require('braintree');

var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "gtm6zprtm2n4mc9z",
  publicKey: "8cqxj3y77hr44yj2",
  privateKey: "8ae0dab9e654fad7296623f77247d91c"
});

router.get("/client_token", function(req, res) {
  console.log(req.query);
  gateway.clientToken.generate({}, function(err, response) {
    console.log(response);
    return res.json(response);
  });
});


router.post("/purchases", function(req, res) {
  var nonce = req.body.paymentMethodNonce;
  var total = req.body.total;
  // Use payment method nonce here
  gateway.transaction.sale({
    amount: total,
    paymentMethodNonce: nonce,
  }, function(err, result) {
    if (err) {
      return res.status(500).json({
        error: err
      });
    } else {
      return res.status(200).json({
        result: result
      })
    }
  });
});
module.exports = router;
