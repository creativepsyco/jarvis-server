var mongoose = require('mongoose');
var aI = require('mongoose-auto-increment');

var UserSchema = new mongoose.Schema({
  email: String,
  name: String,
  password: String,
  phone: String,
  gcm_token: String,
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: Date
});

UserSchema.statics.findByEmailAndPassword = function(email, password) {
  return this.find({
    email: email,
    password: password
  }).exec();
}

aI.initialize(mongoose.connection);
UserSchema.plugin(aI.plugin, {
  model: 'User',
  field: 'userId',
  startAt: 1
});


UserSchema.pre('save', function(next) {
  this.updated_at = new Date();
  next();
});

module.exports = mongoose.model('User', UserSchema);
