var mongoose = require('mongoose');
var crypto = require('crypto');
var aI = require('mongoose-auto-increment');

var TokenSchema = new mongoose.Schema({
  value: String,
  user_id: Number,
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: Date
});


TokenSchema.statics.generate = function() {
  return crypto.randomBytes(32).toString('hex');
}

var aI = require('mongoose-auto-increment');
TokenSchema.plugin(aI.plugin, {
  model: 'Token',
  field: 'tokenId'
});

TokenSchema.pre('save', function(next) {
  this.updated_at = new Date();
  next();
});

module.exports = mongoose.model('Token', TokenSchema);
