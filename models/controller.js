var mongoose = require('mongoose');
var aI = require('mongoose-auto-increment');

var ControllerSchema = new mongoose.Schema({
  careTaker: Number,
  name: String,
  uuid: String,
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: Date
});

aI.initialize(mongoose.connection);
ControllerSchema.plugin(aI.plugin, 'Controller');

ControllerSchema.pre('save', function(next) {
  this.updated_at = new Date();
  next();
});

module.exports = mongoose.model('Controller', ControllerSchema);
