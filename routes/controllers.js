var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Controller = require('../models/controller.js');
var auth = require('../auth.js');
var Jar = require('../models/jar.js');

/* GET controller listing. */
router.get('/', function(req, res, next) {
  Controller.findAsync().then(function(controllers) {
    return res.json(controllers);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

/* GET Single controller */
router.get('/:uuid', function(req, res, next) {
  Controller.findOneAsync({
    uuid: req.params.uuid
  }).then(function(controller) {
    console.log(controller);
    return res.json(controller);
  }).catch(function(err) {
    return next(err);
  });
});

/**
 * Create a new controller
 */
router.post('/', function(req, res, next) {
  Controller.findOneAsync({
    uuid: req.body.uuid
  }).then(function(jar) {
    if (jar != null) {
      return res.json(jar);
    } else {
      Controller.createAsync(req.body).then(function(post) {
        return res.json(post);
      });
    }
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

module.exports = router;
