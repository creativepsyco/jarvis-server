var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Patient = require('../models/patient.js');
var Controller = require('../models/controller.js');
var auth = require('../auth.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  Patient.findAsync().then(function(users) {
    return res.json(users);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

router.get('/:id', function(req, res, next) {
  Patient.findOne({
      patientId: req.params.id
    }).lean().execAsync()
    .then(function(user) {
      if (user == null) {
        return res.status(404).json({
          error: 'Not found for patient id'
        });
      }
      return res.json(user);
    }).catch(function(err) {
      console.error(err);
    });
});

router.post('/', function(req, res, next) {
  Patient.findOneAsync({
    name: req.body.name
  }).then(function(user) {
    if (user != null) {
      return res.json(user);
    } else {
      Patient.createAsync(req.body).then(function(post) {
        return res.json(post);
      });
    }
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

module.exports = router;
