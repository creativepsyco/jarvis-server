var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Patient = require('../models/patient.js');
var Controller = require('../models/controller.js');
var auth = require('../auth.js');
var Jar = require('../models/jar.js');
var User = require('../models/user.js');

router.post('/', function(req, res, next) {
  var controllerArgs = req.body.controller;
  var jarArgs = req.body.jar;
  console.log(jarArgs);

  Controller.findOneAsync({
    uuid: controllerArgs.uuid
  }).then(function(controller) {
    if (controller != null) {
      console.log(controller._id);
      jarArgs.controllerId = controller._id;
      console.log(jarArgs);
      Jar.findOneAsync({
        uuid: jarArgs.uuid
      }).then(function(jar) {
        if (jar == null) {
          Jar.createAsync(jarArgs).then(function(aJar) {
            return res.json(aJar);
          });
        } else {
          var previousWeight = jar.weight;
          jar.weight = jarArgs.weight;
          jar.saveAsync().then(function(aJar) {
            dispatch(controller, jar, previousWeight);
            return res.json(aJar);
          });
        }
      }).catch(function(err) {
        console.error(err);
        return next(err);
      });
    } else {
      Controller.createAsync(controllerArgs).then(function(controller) {
        jarArgs.controllerId = controller._id;
        Jar.findOneAsync({
          uuid: jarArgs.uuid
        }).then(function(jar) {
          if (jar != null) {
            Jar.updateAsync(jarArgs).then(function(aJar) {
              return res.json(aJar);
            });
          } else {
            Jar.createAsync(jarArgs).then(function(aJar) {
              return res.json(aJar);
            });
          }
        });
      })
    }
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

var gcm = require('node-gcm');

function dispatch(controller, data, previousWeight) {
  console.info("previousWeight " + previousWeight)
  var prevTabletCount = previousWeight / data.medicine.weight;
  var tabletCount = data.weight / data.medicine.weight;
  if (tabletCount < 10 && prevTabletCount > 10) {
    console.info("=== Dispatching GCM ====");
    var message = new gcm.Message();
    message.addData('text', 'The Supply on your jar is Running Out');
    User.findOneAsync({
      userId: controller.careTaker
    }).then(function(user){
      var regIds = [user.gcm_token];
      var sender = new gcm.Sender('AIzaSyCcT1A3QW_nYzLhTBEl2nYC3ySOZ4Jxv74');

      sender.send(message, regIds, function(err, result) {
        if (err) console.error(err);
        else console.log(result);
      });
    })
  }
}
router.get('/', function(req, res, next) {
  Jar.findAsync().then(function(jars) {
    return res.json(jars);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

module.exports = router;
