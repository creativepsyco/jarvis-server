var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/user.js');

router.post('/', function(req, res, next) {
  User.findOneAsync({
    userId: req.body.userId
  }).then(function(user) {
    user.gcm_token = req.body.gcm_token;
    user.saveAsync().then(function(user) {
      return res.status(200).json(user);
    }).catch(function(err) {
      return res.status(500).json({
        error: err
      });
    })
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

module.exports = router;
