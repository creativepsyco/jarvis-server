var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Token = require('../models/token.js');
var User = require('../models/user.js');
var auth = require('../auth.js');

/* GET /tokens listing. */
router.get('/', auth.authenticate(), function(req, res, next) {
  Token.find(function(err, todos) {
    if (err) return next(err);
    res.json(todos);
  });
});


/* Log in == create Token */
router.post('/', function(req, res, next) {
  User.findByEmailAndPassword(req.body.email, req.body.password)
    .then(function(users) {
      if (users.length == 1) {
        // Create token or return already existing one
        var user = users[0];
        Token.findAsync({
          user_id: user.userId
        }).then(function(tokens) {
          if (tokens.length == 0) {
            var token_args = {
              value: Token.generate(),
              user_id: user.userId
            }
            Token.createAsync(token_args).then(function(post) {
              return res.json(post);
            });
          } else {
            tokens[0].save();
            return res.json(tokens[0]);
          }
        }).catch(function(err) {
          console.error(err);
          return next(err);
        })
      } else {
        return res.status(404).json({
          error: 'User does not exist'
        });
      }
    });
});


module.exports = router;
