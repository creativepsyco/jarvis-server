var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Jar = require('../models/jar.js');
var auth = require('../auth.js');

/* GET jars listing. */
router.get('/', function(req, res, next) {
  Jar.findAsync().then(function(jars) {
      return res.json(jars);
    }).catch(function(err) {
      console.error(err);
      return next(err);
    });
});

/**
 * Get a jar by jar id
 */
router.get('/:id', function(req, res, next) {
  Jar.findOneAsync({
    jarId: req.params.id
  }).then(function(jar) {
    return res.json(jar);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

router.get('/patient/:id', function(req, res, next) {
  Jar.findAsync({
    patientId: req.params.id
  }).then(function(jars) {
    return res.json(jars);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

/**
 * Create a new jar
 */
router.post('/', function(req, res, next) {
  Jar.findOneAsync({
    uuid: req.body.uuid
  }).then(function(jar) {
    if (jar != null) {
      return res.json(jar);
    } else {
      Jar.createAsync(req.body).then(function(post) {
        return res.json(post);
      });
    }
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});


/*
 * Update a Jar
 */

router.post('/:uuid', function(req, res, next) {
  Jar.findOneAndUpdateAsync({
    uuid: req.params.uuid
  }, req.body, {
    upsert: true,
    new: true
  }).then(function(jar) {
    return res.json(jar);
  }).catch(function(err) {
    console.error(err);
    return next(err);
  });
});

module.exports = router;
